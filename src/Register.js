import React from 'react';
import './App.css';



class Register extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      fields:{firstname:'',lastname:'',gender:'',username:'',password:''},
      errors:{}
    } 
    this.handleChange=this.handleChange.bind(this);
    this.handleSubmit=this.handleSubmit.bind(this);
    
  };
  handleChange(event){
    let fields=this.state.fields;
    fields[event.target.name]=event.target.value;
    this.setState({
      fields
    });
  }
  handleSubmit (event) {
    event.preventDefault();
    if(this.validateForm()){
    let fields={};
    fields["firstname"]="";
    fields["lastname"]="";
    fields["gender"]="";
    fields["username"]="";
    fields["password"]="";
    this.setState({fields:fields});
    alert("form submited")
  }
}
validateForm(){
   let fields= this.state.fields;
   let errors={};
   let formIsValid=true;
  

  if(!fields["firstname"]){
    formIsValid=false;
    errors["firstname"]="please enter firstname"
}
if(typeof ["firstname"] !=="undefined"){
    
  if(!fields["firstname"].match(/^[a-zA-Z]*$/)){
      
      formIsValid=false;
      errors["firstname"]="firstname must be alphabet characters only"
  }
}
if(!fields["lastname"]){
  formIsValid=false;
  errors["lastname"]="please enter lastname"
}
if(typeof ["lastname"] !=="undefined"){
  
if(!fields["lastname"].match(/^[a-zA-Z]*$/)){
    
    formIsValid=false;
    errors["lastname"]="lastname must be alphabet characters only"
}
}
if(!fields["gender"]){
  formIsValid=false;
  errors["gender"]="please enter gender"
}
if(typeof ["gender"] !=="undefined"){
  
if(!fields["gender"].match(/^[a-zA-Z]*$/)){
    
    formIsValid=false;
    errors["gender"]="gender must be alphabet characters only"
}
}

      if(!fields["username"]){
        formIsValid=false;
        errors["username"]="please enter username"
    }
    if(typeof ["username"] !=="undefined"){
        
      if(!fields["username"].match(/^[a-zA-Z]*$/)){
          
          formIsValid=false;
          errors["username"]="username must be alphabet characters only"
      }
    }
    if(!fields["password"]){
    formIsValid=false;
    errors["password"]="please enter password"
    }
    if(typeof fields["password"] !=="undefined"){
    if(!fields["password"].match(/^[0-9]*$/)){
        formIsValid=false
        errors["password"]="password must contain numbers only"
    }
    }
    this.setState({
    errors:errors
    })
    return formIsValid;
    }
  

  render(){
    return(
      <div id="register-form-container">
             <div id="Register">
                 <h2>Register form</h2>
                 <form onSubmit={this.handleSubmit}>
                 <label>firstname</label>
                     <input type="text" name="firstname" value={this.state.fields.firstname}
                     onChange={this.handleChange}/>
                     <div className="errorMsg">{this.state.errors.firstname}</div>
                     <br></br>
                     <label>lastname</label>
                     <input type="text" name="lastname" value={this.state.fields.lastname}
                     onChange={this.handleChange}/>
                     <div className="errorMsg">{this.state.errors.lastname}</div>
                     <br></br>
                     <label>             gender       :</label>
                     <input type="gender" name="gender" value={this.state.fields.gender}
                     onChange={this.handleChange}/>
                     <div className="errorMsg">{this.state.errors.gender}</div>
                     <br></br>
                     <label>username</label>
                     <input type="text" name="username" value={this.state.fields.username}
                     onChange={this.handleChange}/>
                     <div className="errorMsg">{this.state.errors.username}</div>
                     <br></br>
                     <label>password</label>
                     <input type="password" name="password" value={this.state.fields.password}
                     onChange={this.handleChange}/>
                     <div className="errorMsg">{this.state.errors.password}</div>
                     <br></br>
                     <input type="submit" className="button" value="register"/>
                 </form>
                 </div>  
         </div>
    );
  }
}



    

export default Register;
