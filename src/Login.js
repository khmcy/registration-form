import React from 'react';
import  './App.css';


class Login extends React.Component{
    constructor(props) {
        super(props);

        this.state ={
            fields: {username: '',password: ''},
            errors: {}
        }
        this.handleChange=this.handleChange.bind(this);
        this.handleSubmit= this.handleSubmit.bind(this);
    };
      handleChange(event){
          let fields=this.state.fields;
          fields[event.target.name]=event.target.value;
          this.setState({
              fields
          });
      }
       handleSubmit(event){
        event.preventDefault();
         if(this.validateForm()) {
             let fields={};
             fields["username"] = "";
             fields["password"] ="";
             this.setState({fields:fields});
             alert("form submited")
             
         }
       }
       validateForm() {
           let fields=this.state.fields;
           let errors={};
           let formIsValid=true;

            if(!fields["username"]){
               formIsValid=false;
               errors["username"]="please enter username"
           }
            if(typeof ["username"] !=="undefined"){
               
              if(!fields["username"].match(/^[a-zA-Z]*$/)){
                  
                  formIsValid=false;
                  errors["username"]="username must be alphabet characters only"
              }
            }
            if(!fields["password"]){
            formIsValid=false;
            errors["password"]="please enter password"
            }
            if(typeof fields["password"] !=="undefined"){
            if(!fields["password"].match(/^[0-9]*$/)){
               formIsValid=false
               errors["password"]="password must contain numbers only"
            }
        }
        this.setState({
            errors:errors
        })
        return formIsValid;
        }
           
    render() {
        return(
         <div id="login-form-container">
             <div id="Login">
                 <h2>Login form</h2>
                 <form onSubmit={this.handleSubmit}>
                     <label>username</label>
                     <input type="text" name="username" value={this.state.fields.username}
                     onChange={this.handleChange}/>
                     <div className="errorMsg">{this.state.errors.username}</div>
                     <br></br>
                     <label>password</label>
                     <input type="password" name="password" value={this.state.fields.password}
                     onChange={this.handleChange}/>
                     <div className="errorMsg">{this.state.errors.password}</div>
                     <br></br>
                     <input type="submit" className="button" value="login"/>
                 </form>
                 </div>  
         </div>
        );
    
    }
}



export default Login;
