import React from 'react';
import { BrowserRouter as Router,Route} from 'react-router-dom';
import './App.css';
import Nav from './Nav';
import Register from './Register';
import Login from './Login';
import About from './About';

class App extends React.Component {
  render(){
    return(
      <Router>
      <div>
        <h2>User Management System</h2>
        <Nav/>
        <Route path="/" exact component={Home}/>
         <Route path="/Login" component={Login}/>
         <Route path="/Register" component={Register}/>
         <Route path="/About" component={About}/>
      </div>
      </Router>
    );
  }
}
  class Home extends React.Component{
     render(){
       return(
          <div>
          <h1>Home</h1>
          <h2>User Information desk</h2>
          
          </div>
        
       );
     }
  }


export default App;