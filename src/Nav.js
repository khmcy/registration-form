import React from 'react';
import { Link } from 'react-router-dom';


class Nav extends React.Component{
    
    render() {
        const navStyle={
            color: 'white'
        }
        return(
            <nav>
                <ul className="nav-links">
                    <Link style={navStyle} to="/Login">
                        <li>Login</li>
                    </Link>
                    <Link style={navStyle} to="Register">
                        <li>Register</li>
                    </Link>
                    <Link style={navStyle} to="About">
                        <li>About</li>
                    </Link>
          
                </ul> 
            </nav>
        );
    }
}


export default Nav;